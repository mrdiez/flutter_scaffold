
import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutterscaffold/constants/mocks/users.dart';
import 'package:test/test.dart';

import 'test.model.dart';

// Run test with : flutter drive --target=test_driver/main.dart --driver=test_driver/home_test.dart --dart-define=TESTS=true

void main() {
  group('Home Test', () {
    FlutterDriver driver;
    int testNo = 0;

    // Connect to the Flutter driver before running any tests.
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    // Close the connection to the driver after the tests have completed.
    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('#${testNo++} : existing user', () async {
      await HomeTest(TestDataModel(driver, MockedUsers[0])).start();
    });

    test('#${testNo++} : another existing user', () async {
      await HomeTest(TestDataModel(driver, MockedUsers[1])).start();
    });
  }, timeout: Timeout(Duration(seconds: 60)));
}

class HomeTest {

  FlutterDriver get driver => model.driver;
  final TestDataModel model;

  HomeTest(this.model);

  static void run() => main();

  Future<void> start([DateTime testStartingDate]) async {
    await model.login();

    // TODO: implement tests

    await model.logout();
  }
}
