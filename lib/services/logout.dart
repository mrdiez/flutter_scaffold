
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/services/route.dart';
import 'package:flutterscaffold/tools/log.dart';

import 'cache.dart';

class LogoutService {

  static exit(BuildContext context) async {

    Log.print('Logging out', title: 'LogoutService');

    CacheService(context).get.empty();

    RouteService(context).goTo(Routes.Login, params: {RouteParams.LoggedOut: true}, removeUntil: true);
  }
}