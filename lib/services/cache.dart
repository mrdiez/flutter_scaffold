
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/constants/config/storage.keys.dart';
import 'package:flutterscaffold/models/pixabay.image.data.dart';
import 'package:flutterscaffold/services/local.storage.dart';
import 'package:flutterscaffold/tools/array.dart';
import 'package:flutterscaffold/models/user.data.dart';
import 'package:mvcprovider/mvcprovider.dart';

class CacheService extends MVC_Notifier<CacheService> {

  CacheService([context]) : super(context);

  // USERS
  int _loggedUserId;
  int get loggedUserId => _loggedUserId;
  set loggedUserId(int value) {
    _loggedUserId = value;
    notifyListeners();
  }
  Map<int, UserDataModel> users = {};
  List<UserDataModel> get userList => ArrayTool.mapToList<UserDataModel>(users);
  bool get isLogged => loggedUserId != null && users.containsKey(loggedUserId);
  UserDataModel get loggedUser => isLogged ? users[loggedUserId] : null;
  String get sessionToken => loggedUser?.sessionToken;

  void addUsers(List<UserDataModel> _users) {
    users.addEntries(_users.map((user) => MapEntry(user.id, user)));
  }

  // PIXABAY IMAGES
  Map<String, Map<int, PixabayImageDataModel>> imagesByCategory = {};
  bool isImageList(String imageCategory) => ArrayTool.notEmpty(imagesByCategory[imageCategory]);
  Map<int, PixabayImageDataModel> get images => imagesByCategory.values.reduce((a, b) => {...a, ...b});

  void setImages(String imageCategory, List<PixabayImageDataModel> _images) {
    imagesByCategory[imageCategory] = Map.fromIterable(_images, key: (image) => image.id, value: (image) => image);
  }

  // PIXABAY QUERIES
  List<String> pixabayQueries;
  Future<List<String>> getPixabayQueries() async {
    pixabayQueries = await LocalStorageService.get(StorageKeys.PixabayQueries);
    pixabayQueries ??= Routes.Pixabay.keys.toList();
    return pixabayQueries;
  }

  Future<void> addPixabayQuery(String query) async {
    pixabayQueries.add(query);
    Routes.Pixabay.addEntries([Routes.getPixabayRouteMap(query)]);
    await LocalStorageService.store(StorageKeys.PixabayQueries, pixabayQueries);
    notifyListeners();
  }

  Future<void> removePixabayQuery(String query) async {
    pixabayQueries.remove(query);
    Routes.Pixabay.remove(query);
    await LocalStorageService.store(StorageKeys.PixabayQueries, pixabayQueries);
    notifyListeners();
  }

  // CLEAR CACHE
  void empty() {
    _loggedUserId = null;
    users = {};
    imagesByCategory = {};
  }

}