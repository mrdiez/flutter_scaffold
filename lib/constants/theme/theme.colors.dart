// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';

abstract class ThemeColors {
  // This constructor prevents instantiation and extension.
  // ignore: unused_element
  ThemeColors._();

  // MAIN COLORS

  static Color Primary = _Primary;
  static Color PrimaryVariant = _PrimaryVariant;

  static Color Secondary = _Secondary;
  static Color SecondaryVariant = _SecondaryVariant;

  static const Color _Primary = const Color.fromRGBO(0, 255, 190, 1);
  static const Color _PrimaryVariant = const Color.fromRGBO(0, 130, 95, 1);

  static const Color _Secondary = const Color.fromRGBO(170, 25, 225, 1);
  static const Color _SecondaryVariant = const Color.fromRGBO(125, 0, 175, 1);

  static const Color _Tertiary = const Color.fromRGBO(237, 176, 38, 1);
  static const Color _TertiaryVariant = const Color.fromRGBO(175, 125, 40, 1);


  // CUSTOM THEMES COLORS

  static Map<String, Function> Switcher = {
    "Turquoise/Purple": () => _switcher(_Primary, _PrimaryVariant, _Secondary, _SecondaryVariant),
    "Purple/Yellow": () => _switcher(_Secondary, _SecondaryVariant, _Tertiary, _TertiaryVariant),
    "Yellow/Turquoise": () => _switcher(_Tertiary, _TertiaryVariant, _Primary, _PrimaryVariant),
  };

  static void _switcher(Color primary, Color primaryVariant, Color secondary, Color secondaryVariant) {
    Primary = primary;
    PrimaryVariant = primaryVariant;
    Secondary = secondary;
    SecondaryVariant = secondaryVariant;
  }

  // COMMON COLORS

  // Actions
  static const Color Positive = Color.fromRGBO(53, 223, 144, 1);
  static const Color Negative = Color.fromRGBO(211, 47, 47, 1);

  // 50 shades of grey
  static const Color Light = Colors.white;
  static const Color Dark = Colors.black;
  static const Color LightestGrey = Color.fromRGBO(237, 237, 237, 1);
  static const Color LightGrey = Color.fromRGBO(210, 210, 210, 1);
  static const Color MediumGrey = Color.fromRGBO(150, 150, 150, 1);
  static const Color DarkGrey = Color.fromRGBO(75, 75, 75, 1);
  static const Color DarkestGrey = Color.fromRGBO(30, 30, 30, 1);

  // Disabled
  static Color DisabledColor = LightGrey.withOpacity(0.95);
  static Color DisabledFontColor = DarkGrey.withOpacity(0.5);

  // Misc
  static Color TooltipColor = DarkGrey.withOpacity(0.75);
  static const Color SubtitleTextColor = Colors.grey;
  static const Color DefaultShadowColor =  Colors.white54;
  static Color get Divider => Primary;
  static Color get TitleUnderline => Primary;
  static Color get Toast => Primary;

}
