
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/services/theme.dart';

abstract class ThemeSizes {
  // This constructor prevents instantiation and extension.
  // ignore: unused_element
  ThemeSizes._();

  // PADDING

  static const double _PaddingDefaultValue = 20;
  static final Map<Sizes, double> paddingValue = ThemeService.getSizes<double>(_PaddingDefaultValue);
  static final Map<Sizes, EdgeInsets> padding = ThemeService.getSizes<EdgeInsets>(_PaddingDefaultValue, (double value) => EdgeInsets.all(value));
  static final Map<Sizes, Map<Sizes, EdgeInsets>> paddingSymmetric = ThemeService.getSizes<Map<Sizes, EdgeInsets>>(_PaddingDefaultValue, (double horizontal) =>
      ThemeService.getSizes<EdgeInsets>(_PaddingDefaultValue, (double vertical) =>
          EdgeInsets.symmetric(horizontal: horizontal, vertical: vertical)));

  static final Map<AxisDirection, Map<Sizes, EdgeInsets>> paddingOnly = {
    AxisDirection.up: ThemeService.getSizes<EdgeInsets>(_PaddingDefaultValue, (double value) => EdgeInsets.only(top: value)),
    AxisDirection.down: ThemeService.getSizes<EdgeInsets>(_PaddingDefaultValue, (double value) => EdgeInsets.only(bottom: value)),
    AxisDirection.left: ThemeService.getSizes<EdgeInsets>(_PaddingDefaultValue, (double value) => EdgeInsets.only(left: value)),
    AxisDirection.right: ThemeService.getSizes<EdgeInsets>(_PaddingDefaultValue, (double value) => EdgeInsets.only(right: value)),
  };

  // BORDER-RADIUS

  static const double _BorderRadiusDefaultValue = 20;
  static final Map<Sizes, double> borderRadiusValue = ThemeService.getSizes<double>(_BorderRadiusDefaultValue);
  static final Map<Sizes, BorderRadius> borderRadius = ThemeService.getSizes<BorderRadius>(_BorderRadiusDefaultValue, (double value) => BorderRadius.all(Radius.circular(value)));
  static final Map<Sizes, Map<Sizes, BorderRadius>> borderRadiusHorizontal = ThemeService.getSizes<Map<Sizes, BorderRadius>>(_BorderRadiusDefaultValue, (double left) =>
      ThemeService.getSizes<BorderRadius>(_BorderRadiusDefaultValue, (double right) =>
          BorderRadius.only(topLeft: Radius.circular(left), bottomLeft: Radius.circular(left), topRight: Radius.circular(right), bottomRight: Radius.circular(right))));
  static final Map<Sizes, Map<Sizes, BorderRadius>> borderRadiusVertical = ThemeService.getSizes<Map<Sizes, BorderRadius>>(_BorderRadiusDefaultValue, (double top) =>
      ThemeService.getSizes<BorderRadius>(_BorderRadiusDefaultValue, (double bottom) =>
          BorderRadius.only(topLeft: Radius.circular(top), bottomLeft: Radius.circular(bottom), topRight: Radius.circular(top), bottomRight: Radius.circular(bottom))));

  // TEXT

  static const double _TextDefaultSize = 20;
  static final Map<Sizes, double> text = ThemeService.getSizes<double>(_TextDefaultSize);

  // BUTTON

  static const double _ButtonDefaultHeight = 50;
  static const double _ButtonDefaultWidth = 130;
  static final Map<Sizes, double> buttonHeight = ThemeService.getSizes<double>(_ButtonDefaultHeight);
  static final Map<Sizes, double> buttonWidth = ThemeService.getSizes<double>(_ButtonDefaultWidth);

  // INPUT

  static const double _InputDefaultHeight = 50;
  static final Map<Sizes, double> inputHeight = ThemeService.getSizes<double>(_InputDefaultHeight);

  // ICON

  static const double _IconDefaultSize = 30;
  static final Map<Sizes, double> icon = ThemeService.getSizes<double>(_IconDefaultSize);

  // CHECKBOX

  static const double _CheckboxDefaultSize = 50;
  static final Map<Sizes, double> checkbox = ThemeService.getSizes<double>(_CheckboxDefaultSize);

  // SHADOW

  static const double _ShadowDefaultSize = 8;
  static final Map<Sizes, double> shadow = ThemeService.getSizes<double>(_ShadowDefaultSize);

  // IMAGE

  static const double _ImageDefaultSize = 250;
  static final Map<Sizes, double> image = ThemeService.getSizes<double>(_ImageDefaultSize);

  // DIVIDER

  static const double _DividerDefaultSize = 2;
  static final Map<Sizes, double> divider = ThemeService.getSizes<double>(_DividerDefaultSize);

  // SLIVER

  static double getSliverMaxHeight(BuildContext context) => MediaQuery.of(context).size.height / 3;
}

enum Sizes { Zero, XXXS, XXS, XS, S, M, L, XL, XXL, XXXL }

const Map<Sizes, double> SizeRatios = {
  Sizes.Zero : 0,
  Sizes.XXXS : 8/13 * 5/8 * 3/5 * 1/3,
  Sizes.XXS : 8/13 * 5/8 * 3/5,
  Sizes.XS : 8/13 * 5/8,
  Sizes.S : 8/13,
  Sizes.M : 1,
  Sizes.L : 21/13,
  Sizes.XL : 21/13 * 34/21,
  Sizes.XXL : 21/13 * 34/21 * 55/34,
  Sizes.XXXL : 21/13 * 34/21 * 55/34 * 89/55,
};

const Map<Sizes, double> ScreenWidths = {
  Sizes.Zero : 0,
  Sizes.XXXS : 260,
  Sizes.XXS : 400,
  Sizes.XS : 500,
  Sizes.S : 640,
  Sizes.M : 720,
  Sizes.L : 900,
  Sizes.XL : 1080,
  Sizes.XXL : 1440,
  Sizes.XXXL : 2160,
};

const Map<Sizes, double> ScreenHeights = {
  Sizes.Zero : 0,
  Sizes.XXXS : 260,
  Sizes.XXS : 400,
  Sizes.XS : 500,
  Sizes.S : 640,
  Sizes.M : 720,
  Sizes.L : 900,
  Sizes.XL : 1080,
  Sizes.XXL : 1440,
  Sizes.XXXL : 2160,
};