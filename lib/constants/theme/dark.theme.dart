// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';
import 'package:flutterscaffold/constants/theme/theme.colors.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';

import 'main.theme.dart';

ThemeData get DarkTheme => MainTheme.copyWith(
    brightness: Brightness.dark,
    primaryColor: ThemeColors.PrimaryVariant,
    primaryColorLight: ThemeColors.Primary,
    primaryColorDark: ThemeColors.PrimaryVariant,
    accentColor: ThemeColors.SecondaryVariant,
    cardColor: ThemeColors.DarkGrey,
    canvasColor: ThemeColors.DarkestGrey,
    inputDecorationTheme: MainTheme.inputDecorationTheme.copyWith(
        fillColor: ThemeColors.DarkestGrey,
    ),
    colorScheme: MainTheme.colorScheme.copyWith(
        onPrimary: ThemeColors.Light
    ),
    iconTheme: MainTheme.iconTheme.copyWith(
        color: ThemeColors.Secondary
    ),
    chipTheme: MainTheme.chipTheme.copyWith(
        backgroundColor: ThemeColors.SecondaryVariant,
        labelStyle: MainTheme.chipTheme.labelStyle.copyWith(
            color: ThemeColors.Light
        )
    ),
    tabBarTheme: MainTheme.tabBarTheme.copyWith(
        indicator: BoxDecoration(
            border: Border(
                bottom: BorderSide(
                    color: ThemeColors.Secondary,
                    width: ThemeSizes.divider[Sizes.M],
                ),
            ),
        ),
        labelStyle: MainTheme.tabBarTheme.labelStyle.copyWith(
            color: ThemeColors.Light
        ),
    ),
    buttonTheme: MainTheme.buttonTheme.copyWith(
        buttonColor: ThemeColors.PrimaryVariant,
    ),
    buttonColor: ThemeColors.PrimaryVariant,
    indicatorColor: ThemeColors.SecondaryVariant,
    toggleableActiveColor: ThemeColors.PrimaryVariant,
    textTheme: MainTheme.textTheme.copyWith(
        overline: MainTheme.textTheme.overline.copyWith(
            color: ThemeColors.LightGrey,
        ),
        caption: MainTheme.textTheme.caption.copyWith(
            color: ThemeColors.Light,
        ),
        button: MainTheme.textTheme.button.copyWith(
            color: ThemeColors.Dark,
        ),
        bodyText1: MainTheme.textTheme.bodyText1.copyWith(
            color: ThemeColors.Light,
        ),
        bodyText2: MainTheme.textTheme.bodyText2.copyWith(
            color: ThemeColors.Light,
        ),
        subtitle1: MainTheme.textTheme.subtitle1.copyWith(
            color: ThemeColors.Light,
        ),
        subtitle2: MainTheme.textTheme.subtitle2.copyWith(
            color: ThemeColors.Light,
        ),
        headline1: MainTheme.textTheme.headline1.copyWith(
            color: ThemeColors.Primary,
        ),
        headline2: MainTheme.textTheme.headline2.copyWith(
            color: ThemeColors.Primary,
        ),
        headline3: MainTheme.textTheme.headline3.copyWith(
            color: ThemeColors.Primary,
        ),
        headline4: MainTheme.textTheme.headline4.copyWith(
            color: ThemeColors.Primary,
        ),
        headline5: MainTheme.textTheme.headline5.copyWith(
            color: ThemeColors.Primary,
        ),
        headline6: MainTheme.textTheme.headline6.copyWith(
            color: ThemeColors.Primary,
        ),
    )
);
