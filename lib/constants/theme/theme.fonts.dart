// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';
import 'package:flutterscaffold/constants/theme/main.theme.dart';

abstract class ThemeFonts {
  // This constructor prevents instantiation and extension.
  // ignore: unused_element
  ThemeFonts._();

  static TextStyle Link = MainTheme.textTheme.subtitle2.copyWith(
    color: Colors.blue,
    decoration: TextDecoration.underline
  );
}