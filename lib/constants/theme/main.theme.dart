// ignore_for_file: non_constant_identifier_names
import 'package:flutter/material.dart';
import 'package:flutterscaffold/constants/theme/theme.colors.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';

ThemeData MainTheme = ThemeData(
  fontFamily: "Cera Pro",
  disabledColor: ThemeColors.DisabledColor,
  buttonTheme: ButtonThemeData(
    textTheme: ButtonTextTheme.primary,
  ),
  tabBarTheme: TabBarTheme(
    labelStyle: TextStyle(
      fontSize: ThemeSizes.text[Sizes.S],
      fontWeight: FontWeight.bold,
    ),
    unselectedLabelStyle: TextStyle(
      fontSize: ThemeSizes.text[Sizes.S],
      fontWeight: FontWeight.normal,
    ),
  ),
  tooltipTheme: TooltipThemeData(
      decoration: BoxDecoration(
          color: ThemeColors.TooltipColor,
          borderRadius: ThemeSizes.borderRadius[Sizes.XS])),
  textTheme: TextTheme(
    overline: TextStyle(
        fontSize: ThemeSizes.text[Sizes.S],
        fontWeight: FontWeight.w500
    ),
    caption: TextStyle(
        fontSize: ThemeSizes.text[Sizes.S],
        fontWeight: FontWeight.w500
    ),
    button: TextStyle(
        fontSize: ThemeSizes.text[Sizes.M]
    ),
    bodyText1: TextStyle(
      fontSize: ThemeSizes.text[Sizes.M],
    ),
    bodyText2: TextStyle(
      fontSize: ThemeSizes.text[Sizes.S],
    ),
    subtitle1: TextStyle(
        fontSize: ThemeSizes.text[Sizes.M],
        fontWeight: FontWeight.bold
    ),
    subtitle2: TextStyle(
        fontSize: ThemeSizes.text[Sizes.M],
        fontWeight: FontWeight.normal
    ),
    headline1: TextStyle(
      fontSize: ThemeSizes.text[Sizes.XXXL],
      fontWeight: FontWeight.bold,
    ),
    headline2: TextStyle(
      fontSize: ThemeSizes.text[Sizes.XXL],
      fontWeight: FontWeight.bold,
    ),
    headline3: TextStyle(
      fontSize: ThemeSizes.text[Sizes.XL],
      fontWeight: FontWeight.bold,
    ),
    headline4: TextStyle(
      fontSize: ThemeSizes.text[Sizes.L],
      fontWeight: FontWeight.bold,
    ),
    headline5: TextStyle(
      fontSize: ThemeSizes.text[Sizes.M],
      fontWeight: FontWeight.bold,
    ),
    headline6: TextStyle(
      fontSize: ThemeSizes.text[Sizes.S],
      fontWeight: FontWeight.bold,
    ),
  ),
);
