// ignore_for_file: non_constant_identifier_names

import 'package:flutterscaffold/constants/config/routes.dart';

Map<String, dynamic> MockedPixabay = Map
    .fromEntries(Routes.Pixabay.keys.map((query) {

      int _i = 0;

      return MapEntry(query, {
        "total": 140959,
        "totalHits": 500,
        "hits": [
          {
            "id": 50267,
            "pageURL": "https://pixabay.com/photos/owl-bird-animal-bird-of-prey-50267/",
            "type": "photo",
            "tags": "Mocked $query ${_i++}, owl, bird, animal",
            "previewURL": "https://cdn.pixabay.com/photo/2012/06/19/10/32/owl-50267_150.jpg",
            "previewWidth": 150,
            "previewHeight": 112,
            "webformatURL": "https://pixabay.com/get/g2ace1d95fb28c1eb4193cd5aa44d7ac393d0a04a0c74f3522edbbb5cb2a20f3405ce0f4aec5e72d5bafce6d7ef23d606_640.jpg",
            "webformatWidth": 640,
            "webformatHeight": 480,
            "largeImageURL": "https://pixabay.com/get/g46656dd82494002897dde7f9b8fc7bc35998e8d928d1c039b516210355a90b669092d70bfea08cae4a3699e3312e77f1_1280.jpg",
            "imageWidth": 2122,
            "imageHeight": 1593,
            "imageSize": 957134,
            "views": 526190,
            "downloads": 146955,
            "favorites": 1586,
            "likes": 1834,
            "comments": 284,
            "user_id": 5555,
            "user": "Chraecker",
            "userImageURL": "https://cdn.pixabay.com/user/2012/06/19/12-38-10-432_250x250.jpg"
          },
          {
            "id": 1979445,
            "pageURL": "https://pixabay.com/photos/iceland-arctic-fox-fox-animal-1979445/",
            "type": "photo",
            "tags": "Mocked $query ${_i++}, iceland, arctic fox, fox",
            "previewURL": "https://cdn.pixabay.com/photo/2017/01/14/12/59/iceland-1979445_150.jpg",
            "previewWidth": 150,
            "previewHeight": 100,
            "webformatURL": "https://pixabay.com/get/g7a40770f30dec708034b429abe0cce9259195042d56b894556988e9a63eeb750cd6add64ba6ad6f33e23683a4495c5d71a348f62efb532b241ed159c7265ab11_640.jpg",
            "webformatWidth": 640,
            "webformatHeight": 427,
            "largeImageURL": "https://pixabay.com/get/g465592d513c2d62d9ebc25f595591396065805775617a90f9b76c53657fbf146530beaec0f32924496168ab68df3d9dada37e8ff68f0b3784c5904a7870903e5_1280.jpg",
            "imageWidth": 2200,
            "imageHeight": 1468,
            "imageSize": 298875,
            "views": 805244,
            "downloads": 406606,
            "favorites": 1740,
            "likes": 1928,
            "comments": 195,
            "user_id": 12019,
            "user": "12019",
            "userImageURL": ""
          },
          {
            "id": 3601194,
            "pageURL": "https://pixabay.com/photos/ara-parrot-yellow-macaw-bird-3601194/",
            "type": "photo",
            "tags": "Mocked $query ${_i++}, ara, parrot, yellow macaw",
            "previewURL": "https://cdn.pixabay.com/photo/2018/08/12/16/59/ara-3601194_150.jpg",
            "previewWidth": 150,
            "previewHeight": 99,
            "webformatURL": "https://pixabay.com/get/g69ab14eea8534142a8cfec08abcb752a0ce365db68bc5e5a0c972e160d27e45f42765da89f64952ff5bc924388a728ae432187ce4e9489477ff28bf2e7e7ef40_640.jpg",
            "webformatWidth": 640,
            "webformatHeight": 426,
            "largeImageURL": "https://pixabay.com/get/ge87d089325d1422b6aa8e44085c12b8ad21e93d5c1dc29e0fb77ffe421cd02de3c763474cfba36edbcd36728056e237b585d6ea6430e0b8ba5107f7d46284238_1280.jpg",
            "imageWidth": 4896,
            "imageHeight": 3264,
            "imageSize": 2922892,
            "views": 648014,
            "downloads": 519121,
            "favorites": 1407,
            "likes": 1437,
            "comments": 210,
            "user_id": 1195798,
            "user": "Couleur",
            "userImageURL": "https://cdn.pixabay.com/user/2021/01/29/20-27-23-715_250x250.jpg"
          },
        ]
      });

}));