// ignore_for_file: non_constant_identifier_names
import 'package:flutterscaffold/models/user.data.dart';

/// To login please use one of these mocked users:
/// E-mail: peter.parker@gmail.com OR lara.croft@gmail.com OR san.goku@gmail.com
/// Password: 1234
List<UserDataModel> MockedUsers = [
  UserDataModel.mocked("Parker", "Peter", "https://cdn.pixabay.com/photo/2013/03/02/02/40/little-89159_1280.jpg"),
  UserDataModel.mocked("Croft", "Lara", "https://cdn.pixabay.com/photo/2019/01/31/15/05/lara-3967128_1280.png"),
  UserDataModel.mocked("Goku", "San", "https://cdn.pixabay.com/photo/2020/07/02/12/41/goku-5362869_1280.jpg"),
];