
const Map<dynamic, String> Assets = {
  Backgrounds.White: "assets/flutter_scaffold/backgrounds/white-3000x3000.png",
  Logos.Vertical: "assets/flutter_scaffold/logos/logo-vertical-2048.png",
  Logos.VerticalDark: "assets/flutter_scaffold/logos/logo-vertical-2048-dark.png",
  Logos.VerticalColor: "assets/flutter_scaffold/logos/logo-vertical-2048-color.png",
  Logos.Horizontal: "assets/flutter_scaffold/logos/logo-horizontal-2048.png",
  Logos.HorizontalDark: "assets/flutter_scaffold/logos/logo-horizontal-2048-dark.png",
  Logos.HorizontalColor: "assets/flutter_scaffold/logos/logo-horizontal-2048-color.png",
  Logos.EasterEgg: "assets/flutter_scaffold/logos/easter-egg.png",
};

enum Backgrounds {
  White,
}

enum Logos {
  Vertical,
  VerticalDark,
  VerticalColor,
  Horizontal,
  HorizontalDark,
  HorizontalColor,
  EasterEgg
}