
const Map<String, String> ENGLISH = {


  //--------------------- LOGIN ------------------------

  "Log in": "Log in",
  "Error, check your connection\nor try again later.": "Error, check your connection\nor try again later.",

  "Remember me": "Remember me",
  "Forgot your password ?": "Forgot your password ?",

  "Reset my password": "Reset my password",
  "Confirm your email :": "Confirm your email :",

  "Cancel": "Cancel",
  "Validate": "Validate",

  "A new password has been sent to you by email.": "A new password has been sent to you by email.",
  "The password associated with the email": "The password associated with the email",
  "could not be reset.": "could not be reset.",



  //--------------------- HOME ------------------------

  "No tag selected, please add some tags by going in Menu/Settings": "No tag selected, please add some tags by going in Menu/Settings",



  //--------------------- PIXABAY ------------------------

  "By": "By",
  "Pixabay's image url expired, please go back to the list image and pull to refresh.":
  "Pixabay's image url expired, please go back to the list image and pull to refresh.",
  "Tags": "Tags",
  "Could not launch url": "Could not launch url",
  "No image found with this tag": "No image found with this tag :/",



  //--------------------- SETTINGS ------------------------

  "Settings": "Settings",
  "Pixabay tags": "Pixabay tags",
  "Theme": "Theme",
  "Light": "Light",
  "Dark": "Dark",



  //--------------------- COMPONENTS ------------------------


  // SIDE MENU (DRAWER)

  "Log out": "Log out",


  // EMAIL TEXT INPUT

  "E-mail": "E-mail",
  "Username": "Username",
  "Invalid e-mail": "Invalid e-mail",


  // PASSWORD TEXT INPUT

  "Password": "Password",
  "characters minimum": "characters minimum",
  "Incorrect credentials": "Incorrect credentials",



  //--------------------- TOOLS ------------------------

  // DATE

  "Tomorrow": "Tomorrow",
  "Today": "Today",
  "Yesterday": "Yesterday",
  "at": "at",
  "day0": "Monday",
  "day1": "Tuesday",
  "day2": "Wednesday",
  "day3": "Thursday",
  "day4": "Friday",
  "day5": "Saturday",
  "day6": "Sunday",



  //--------------------- SERVICES ------------------------

  "Thank you to grant location": "Thank you to grant location permission to our application in your device settings",
  "Thank you to enable location service": "Thank you to enable location service"


};
