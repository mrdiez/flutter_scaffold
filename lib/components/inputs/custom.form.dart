
import 'package:flutter/material.dart';
import 'package:flutterscaffold/components/inputs/custom.text.input.dart';

class CustomForm extends StatefulWidget {

  final Function onSubmit;
  final Function onSubmitFail;
  final bool disableSubmit;
  final Widget child;
  final List<CustomTextInputModel> inputModels;

  CustomForm({
    Key key,
    this.onSubmit,
    this.onSubmitFail,
    this.disableSubmit = false,
    this.inputModels,
    this.child,
  }) : super(key: key);

  @override
  CustomFormState createState() => CustomFormState();
}

class CustomFormState extends State<CustomForm> {

  String errorMsg;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  Function get submit => !widget.disableSubmit ? () {
    if (validateForSubmit() && widget.onSubmit != null) widget.onSubmit();
    else if (widget.onSubmitFail != null) widget.onSubmitFail();
    widget.inputModels.forEach((input) => input.rebuild());
  } : null;

  bool validateForSubmit() {
      formKey?.currentState?.save();
      focusInvalid();
      return widget.inputModels.fold(true, (a, b) => a && b.isValid);
  }

  void validateForChange() {
      formKey.currentState.validate();
      widget.inputModels.forEach((input) => input.rebuild());
  }

  void focusInvalid() {
    removeFocus();
    CustomTextInputModel firstInvalid = widget.inputModels.firstWhere((input) => !input.isValid, orElse: () => null);
    if (firstInvalid != null) FocusScope.of(context).requestFocus(firstInvalid.focusNode);
  }

  void removeFocus() {
    widget.inputModels.forEach((input) => input.focusNode.unfocus());
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      onChanged: validateForChange,
      child: widget.child
    );
  }
}