
import 'package:flutter/material.dart';
import 'package:flutterscaffold/tools/text.dart';

import 'custom.text.input.dart';

class SimpleTextInput extends StatelessWidget {

  SimpleTextInput({ this.testKey, model})
    : this.model = model ?? SimpleTextInputModel();

  final SimpleTextInputModel model;
  final String testKey;

  @override
  Widget build(BuildContext context) {
    return CustomTextInput(model: model, testKey: testKey);
  }
}

class SimpleTextInputModel extends CustomTextInputModel {

  SimpleTextInputModel({
    this.minLength = 2,
    String value,
    dynamic next,
    String label,
    String errorMessage,
    this.icon,
    this.keyboard
  })
      : this.labelToTranslate = label,
        this.errorToTranslate = errorMessage,
        super(value: value, next: next);

  final int minLength;

  @override
  String get label => TextTool.notNull(labelToTranslate)
      ? translate[labelToTranslate] : null;
  @override
  String get invalidMessage => minLength.toString() + " " + translate["characters minimum"];
  @override
  String get errorMessage => TextTool.notNull(errorToTranslate)
      ? translate[errorToTranslate] : null;

  final String labelToTranslate;
  final String errorToTranslate;

  final bool secret = false;
  final bool required = true;
  final int maxLines = 1;
  final TextInputType keyboard;
  final Icon icon;
  final TextAlign textAlign = TextAlign.center;
}
