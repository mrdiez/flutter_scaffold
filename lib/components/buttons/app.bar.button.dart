import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/tools/widget.dart';

class AppBarButton extends StatelessWidget {
  final IconData icon;
  final Color color;
  final Sizes size;
  final EdgeInsets padding;
  final String testKey;
  final Function(BuildContext) action;
  final String hero;

  AppBarButton(
      {this.icon,
      this.color,
      this.size,
      this.padding,
      this.action,
      this.testKey,
      this.hero = "APP_BAR_BUTTON"});

  double get width => _size + _padding.horizontal;
  double get height => _size + _padding.vertical;
  double get _size => ThemeSizes.icon[size ?? Sizes.M];
  EdgeInsets get _padding => padding ?? ThemeSizes.paddingSymmetric[Sizes.Zero][Sizes.S];

  @override
  Widget build(BuildContext context) {
    return WidgetTool.wrapWithHero(
        hero,
        IconButton(
            key: testKey != null ? Key(testKey) : null,
            icon: Icon(icon,
                size: _size,
                color: color ?? Theme.of(context).iconTheme.color
            ),
            constraints: BoxConstraints(
                maxWidth: _size,
                maxHeight: height
            ),
            padding: _padding,
            iconSize: _size,
            onPressed: () => action(context)
        )
    );
  }
}
