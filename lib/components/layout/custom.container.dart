
import 'package:flutterscaffold/constants/theme/theme.colors.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutterscaffold/tools/widget.dart';
import 'package:flutter/material.dart';

class CustomContainer extends StatelessWidget {

  final ShapeBorder shape;
  final BorderRadius radius;
  final Widget child;
  final String heroTag;
  final EdgeInsets margin;
  final Color color;
  final BorderSide border;
  final String testKey;

  CustomContainer({
    this.shape,
    this.radius,
    this.child,
    this.border,
    this.heroTag,
    this.margin,
    this.color,
    this.testKey,
  });

  @override
  Widget build(BuildContext context) {

    return WidgetTool.wrapWithHero(heroTag,
        Card(
          elevation: ThemeSizes.shadow[Sizes.S],
          shadowColor: ThemeColors.DefaultShadowColor,
          color: color ?? Theme.of(context).cardTheme.color,
          margin: margin ?? ThemeSizes.padding[Sizes.Zero],
          shape: shape ?? RoundedRectangleBorder(
              borderRadius: radius ?? ThemeSizes.borderRadius[Sizes.S],
              side: border ?? BorderSide.none
          ),
          child: child,
          key: Key(testKey),
        )
    );
  }

}