
import 'package:flutterscaffold/components/misc/rotate.on.loading.dart';
import 'package:flutterscaffold/constants/config/test.keys.dart';
import 'package:flutterscaffold/constants/theme/theme.colors.dart';
import 'package:flutterscaffold/services/loading.dart';
import 'package:flutter/material.dart';
import 'package:flutterscaffold/services/cache.dart';
import 'package:flutterscaffold/services/layout.dart';
import 'package:flutterscaffold/services/theme.dart';

import 'side.menu.dart';

class RootWidget extends StatelessWidget {

  final Widget child;

  RootWidget({this.child});

  Widget wrapWithLoader(bool shouldWrap, Widget _child) => shouldWrap
      ? RotateOnLoading(
    testKey: TestKey(key: Keys.BackgroundLoader),
    force: shouldWrap,
    child: _child
  ) : _child;

  @override
  Widget build(BuildContext context) {
    LayoutService layout = LayoutService(context).listen;
    bool showSideMenu = CacheService(context).listen.isLogged;
    bool isDark = ThemeService(context).get.isDark;

    return Scaffold(
      key: layout.scaffoldKey,
      body: Stack(
        alignment: Alignment.center,
        children: [
          if (layout.backgroundImagePath != null) wrapWithLoader(
              LoadingService(context).listen.isLoading,
              Transform.scale(
                scale: layout.getBackgroundScaleRatio(context),
                child: Container(
                  decoration: BoxDecoration(
                      color: isDark ? ThemeColors.Dark : null,
                      image: DecorationImage(
                        colorFilter: !isDark ? null
                          : ColorFilter.mode(
                            Colors.black.withOpacity(0.2),
                            BlendMode.dstATop
                        ),
                        image: AssetImage(layout.backgroundImagePath),
                      )
                  ),
                ),
            ),
          ),
          child
        ],
      ),
      drawer: showSideMenu ? SideMenu() : null,
      floatingActionButton: !layout.isKeyboardVisible ? layout.actionButton : null,
    );
  }
}