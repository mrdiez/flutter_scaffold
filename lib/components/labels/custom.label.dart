

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutterscaffold/tools/widget.dart';
import 'package:flutter/material.dart';

class CustomLabel extends StatelessWidget {

  final String text;
  final TextAlign align;
  final Color color;
  final Sizes size;
  final TextStyle style;
  final TextOverflow overflow;
  final bool wrap;
  final String testKey;
  final String heroTag;
  final bool expand;
  final int maxLines;

  CustomLabel(this.text, {
    this.heroTag,
    this.maxLines = 1,
    this.expand = false,
    this.align,
    this.color,
    this.size,
    this.style,
    this.overflow = TextOverflow.visible,
    this.wrap = true,
    this.testKey
  });

  Widget wrapWithExpand(Widget child) =>
      expand ?? false ? Row(
        children: [
          Expanded(child: child)
        ],
      ) : child;

  TextStyle getTextStyle(BuildContext context) {
    return style ?? Theme.of(context).textTheme.bodyText2;
  }

  double getHeight(BuildContext context) => getTextStyle(context).fontSize * (getTextStyle(context).height ?? 1);

  @override
  Widget build(BuildContext context) {
    TextStyle _style = getTextStyle(context);
    return wrapWithExpand(
      WidgetTool.wrapWithHero(heroTag,
        AutoSizeText(text,
          softWrap: wrap,
          overflow: overflow ?? TextOverflow.visible,
          style: _style.copyWith(color: color ?? _style.color),
          textAlign: align ?? TextAlign.left,
          textKey: Key(testKey),
          maxLines: maxLines,
        ),
      ),
    );
  }
}