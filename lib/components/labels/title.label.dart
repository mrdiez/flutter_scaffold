
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutter/material.dart';

import 'custom.label.dart';

class TitleLabel extends CustomLabel {

  TitleLabel(text, {
    heroTag,
    maxLines = 1,
    expand = false,
    align,
    color,
    size,
    overflow = TextOverflow.visible,
    wrap = true,
    testKey
  }) : super(text,
      heroTag: heroTag,
      maxLines: maxLines,
      expand: expand,
      align: align,
      color: color,
      size: size,
      overflow: overflow,
      wrap: wrap,
      testKey: testKey
  );

  @override
  TextStyle getTextStyle(BuildContext context) {
    switch(size) {
      case Sizes.XXL: return Theme.of(context).textTheme.headline1;
      case Sizes.XL: return Theme.of(context).textTheme.headline2;
      case Sizes.L: return Theme.of(context).textTheme.headline3;
      case Sizes.M: return Theme.of(context).textTheme.headline4;
      case Sizes.S: return Theme.of(context).textTheme.headline5;
      case Sizes.XS: return Theme.of(context).textTheme.headline6;
      default: return Theme.of(context).textTheme.headline4;
    }
  }
}