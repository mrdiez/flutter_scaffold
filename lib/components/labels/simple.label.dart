
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutter/material.dart';

import 'custom.label.dart';

class SimpleLabel extends CustomLabel {

  SimpleLabel(text, {
    heroTag,
    maxLines = 1,
    expand = false,
    align,
    color,
    size,
    overflow = TextOverflow.visible,
    wrap = true,
    testKey
  }) : super(text,
      heroTag: heroTag,
      maxLines: maxLines,
      expand: expand,
      align: align,
      color: color,
      size: size,
      overflow: overflow,
      wrap: wrap,
      testKey: testKey
  );

  @override
  TextStyle getTextStyle(BuildContext context) {
    switch(size) {
      case Sizes.L: return Theme.of(context).textTheme.bodyText1;
      case Sizes.M: return Theme.of(context).textTheme.bodyText2;
      default: return Theme.of(context).textTheme.bodyText2;
    }
  }
}