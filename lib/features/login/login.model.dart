
import 'package:flutter/material.dart';
import 'package:flutterscaffold/components/inputs/email.text.input.dart';
import 'package:flutterscaffold/components/inputs/password.text.input.dart';
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/constants/config/storage.keys.dart';
import 'package:flutterscaffold/constants/config/test.keys.dart';
import 'package:flutterscaffold/services/loading.dart';
import 'package:flutterscaffold/services/local.storage.dart';
import 'package:flutterscaffold/services/route.dart';
import 'package:flutterscaffold/services/translation.dart';
import 'package:flutterscaffold/tools/text.dart';
import 'package:mvcprovider/mvcprovider.dart';

import 'login.ctrl.dart';

class LoginModel extends MVC_Model<LoginCtrl> {

  String defaultEmail;
  String defaultPwd;
  String errorMsg;

  PasswordTextInputModel password;
  EmailTextInputModel email;
  EmailTextInputModel forgotPwdEmail;

  bool get isLoggedOut => RouteService(context).get.params[RouteParams.LoggedOut] ?? false;

  bool get isError => errorMsg != null || email?.currentMessage != null || password?.currentMessage != null;

  bool get isPasswordError {
    return isError && password.currentMessage == TranslationService(context).get["Incorrect credentials"];
  }

  bool wantToSaveCredential = false;
  Features loadingId = Features.Login;

  // Init model with function to call on keyboard validation or on auto login (if we're not coming here from a log out)
  @override
  LoginModel create (BuildContext context, LoginCtrl ctrl) {
    checkStoredCredential().then((value) => initInputs());
    return super.create(context, ctrl);
  }

  // Init text inputs models with stored values and with next element to focus on keyboard validation
  void initInputs () {
    password = PasswordTextInputModel(value: defaultPwd, next: wantToSaveCredential ? ctrl.submit : null);
    email = EmailTextInputModel(value: defaultEmail, next: password.focusNode);
  }

  // Check if we previously wanted to save credentials then restore them into text inputs and try to auto login
  Future<void> checkStoredCredential() async {

    if (defaultEmail == null && defaultPwd == null) {

      await setWantToSaveCredential(await LocalStorageService.get<bool>(StorageKeys.SaveCredentials) ?? false);

      if (wantToSaveCredential || isLoggedOut) {

        defaultEmail = await LocalStorageService.get<String>(StorageKeys.Email);
        defaultPwd = await LocalStorageService.get<String>(StorageKeys.Password);

        if (TextTool.notNull(defaultEmail) && TextTool.notNull(defaultPwd) && defaultEmail.length >= 4 && defaultPwd.length >= 4) {
          initInputs();
          if (!isLoggedOut && !LoadingService(context).get.isLoading) ctrl.submit();
          return notifyListeners();
        }
      }
      defaultEmail = "";
      defaultPwd = "";
      notifyListeners();
    }
  }

  // Do we really need to comment this method ?
  Future<void> saveCredential() {
    if (wantToSaveCredential) {
      return LocalStorageService.storeAll({
        StorageKeys.Email: email.value,
        StorageKeys.Password: password.value
      });
    } else {
      return removeSavedCredential();
    }
  }

  // And this one ?
  Future<void> removeSavedCredential() => LocalStorageService.removeAll([StorageKeys.Email, StorageKeys.Password]);

  // Save the boolean "wantToSaveCredential" on local storage when it's value changed
  Future<void> setWantToSaveCredential(bool value) async {
    if (wantToSaveCredential != value) {
      await LocalStorageService.store(StorageKeys.SaveCredentials, value);
      wantToSaveCredential = value;
      if (!value) removeSavedCredential();
      if (password != null)
        password.next = wantToSaveCredential ? ctrl.submit : null;
      notifyListeners();
    }
  }
}