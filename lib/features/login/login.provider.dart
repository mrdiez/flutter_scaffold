
import 'package:flutterscaffold/models/user.data.dart';
import 'package:flutterscaffold/services/http.dart';
import 'package:http/http.dart';

class LoginProvider extends HttpService {
  static final path = "/login";

  LoginProvider(context) : super(context);

  // Should return a UserDataModel instance by calling a ws which should return a json with user data
  Future<UserDataModel> login(String email, String password) async => await this.get<UserDataModel>(
    path: path,
    params: {
      'login': email,
      'password': password,
    },
    headers: {
      "Content-Type": "application/json"
    },
    responseProperty: 'userData',
    method: HTTPMethods.MOCK,
    paramsAsString: true,
    factory: UserDataModel.fromJson,
  );

  // TODO: To be implemented
  // Should return a int, 0 => error, 1 => ok, 2 => OMFG
  Future<int> forgotPassword(String email) async => await HttpRequest<int>(
      path: path + "/forgot",
      params: { 'email': email },
      parser: <int> (Response response, [String responseProperty]) async => double.parse(response.body).toInt() as int
  ).run();
}
