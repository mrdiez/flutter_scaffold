
import 'package:flutter/material.dart';
import 'package:flutterscaffold/components/buttons/ok.button.dart';
import 'package:flutterscaffold/components/buttons/simple.button.dart';
import 'package:flutterscaffold/components/inputs/custom.form.dart';
import 'package:flutterscaffold/components/inputs/select.box.dart';
import 'package:flutterscaffold/components/labels/simple.label.dart';
import 'package:flutterscaffold/components/labels/subtitle.label.dart';
import 'package:flutterscaffold/components/layout/custom.sliver.bar.dart';
import 'package:flutterscaffold/components/layout/sliver.view.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutterscaffold/features/settings/components/query.text.input.dart';
import 'package:flutterscaffold/services/theme.dart';
import 'package:flutterscaffold/tools/text.dart';
import 'package:mvcprovider/mvcprovider.dart';

import 'settings.ctrl.dart';
import 'settings.model.dart';

class SettingsView extends StatelessWidget with MVC_View<SettingsModel, SettingsCtrl>  {

  @override
  Widget build(BuildContext context) {
    listen(context);
    bool isDark = ThemeService(context).listen.isDark;

    return SliverView(
      appBar: CustomSliverBar(
          title: SubtitleLabel(TextTool.capitalize(model.title), heroTag: "TITLE-${model.title}"),
        ),
      child: Padding(
        padding: ThemeSizes.padding[Sizes.L],
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomForm(
              key: ctrl.formKey,
              onSubmit: ctrl.onSubmitQuery,
              inputModels: [
                model.tagInput
              ],
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(child: QueryTextInput(model: model.tagInput)),
                  Padding(
                    padding: ThemeSizes.paddingOnly[AxisDirection.left][Sizes.M],
                    child: OkButton(
                      padding: ThemeSizes.padding[Sizes.Zero],
                      action: () => ctrl.formKey.currentState.submit(),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: ThemeSizes.paddingOnly[AxisDirection.down][Sizes.L],
              child:
              Wrap(
                  children: model.cache.pixabayQueries.map((query) {
                    return Padding(
                      padding: ThemeSizes.padding[Sizes.XXS],
                      child: ActionChip(
                        onPressed: () => ctrl.removeQuery(query),
                        label: Text(query),
                      ),
                    );
                  }).toList()
              ),
            ),
            Padding(
              padding: ThemeSizes.paddingOnly[AxisDirection.down][Sizes.L],
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: ThemeSizes.paddingOnly[AxisDirection.down][Sizes.L],
                    child: SimpleLabel("${model.translate["Theme"]} : ", size: Sizes.L),
                  ),
                  Row(
                    children: [
                      SimpleButton(
                        action: ctrl.onToggleTheme,
                        child: Row(
                          children: [
                            Icon(isDark ? Icons.wb_sunny : Icons.nightlight_round),
                            Padding(
                              padding: ThemeSizes.paddingOnly[AxisDirection.left][Sizes.XS],
                              child: Text(isDark
                                  ? model.translate["Light"]
                                  : model.translate["Dark"]
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        child: Padding(
                          padding: ThemeSizes.paddingOnly[AxisDirection.left][Sizes.M],
                          child: SelectBox<String>(
                              keyValues: model.themes,
                              defaultValue: ThemeService(context).listen.currentTheme,
                              onChanged: ctrl.onThemeSelect,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              )
            ),
          ],
        ),
      ),
    );
  }
}