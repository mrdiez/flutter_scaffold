
import 'package:flutter/cupertino.dart';
import 'package:flutterscaffold/components/inputs/custom.form.dart';
import 'package:flutterscaffold/services/loading.dart';
import 'package:flutterscaffold/services/theme.dart';
import 'package:mvcprovider/mvcprovider.dart';

import 'settings.model.dart';

class SettingsCtrl extends MVC_Controller<SettingsModel> {

  final GlobalKey<CustomFormState> formKey = GlobalKey<CustomFormState>();
  LoadingService get loading => LoadingService(context).get;
  ThemeService get theme => ThemeService(context).get;

  Future<void> onSubmitQuery() async {
    loading.start(model.loadingId);
    await model.cache.addPixabayQuery(model.tagInput.value);
    model.tagInput.value = null;
    loading.stop(model.loadingId);
    notifyListeners();
  }

  Future<void> removeQuery(String query) async {
    loading.start(model.loadingId);
    await model.cache.removePixabayQuery(query);
    model.tagInput.value = null;
    loading.stop(model.loadingId);
    notifyListeners();
  }

  void onToggleTheme() => theme.toggleMode();
  dynamic onThemeSelect(dynamic value) => theme.selectTheme(value);

}