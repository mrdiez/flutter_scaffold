
import 'package:flutterscaffold/constants/config/routes.dart';
import 'package:flutterscaffold/constants/config/test.keys.dart';
import 'package:flutterscaffold/features/pixabay/list/pixabay.list.ctrl.dart';
import 'package:flutterscaffold/models/pixabay.image.data.dart';
import 'package:flutterscaffold/services/cache.dart';
import 'package:flutterscaffold/services/route.dart';
import 'package:mvcprovider/mvcprovider.dart';

class PixabayListModel extends MVC_Model<PixabayListCtrl> {

  final String query;
  List<PixabayImageDataModel> images;
  bool showAppBar = false;
  Features loadingId = Features.Pixabays;

  CacheService get cacheService => CacheService(context).get;

  PixabayListModel(this.query);

  void initAppBar([bool shouldRefresh = false]) {
    showAppBar = RouteService.History.last != Routes.Home;
    if (shouldRefresh) notifyListeners();
  }

  @override
  void init() {
    initAppBar();
    images = cacheService.imagesByCategory[query].values.toList();
  }
}