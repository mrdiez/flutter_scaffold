
import 'package:flutter/material.dart';
import 'package:flutterscaffold/components/labels/subtitle.label.dart';
import 'package:flutterscaffold/components/layout/custom.sliver.bar.dart';
import 'package:flutterscaffold/components/layout/sliver.view.dart';
import 'package:flutterscaffold/constants/theme/theme.sizes.dart';
import 'package:flutterscaffold/features/pixabay/components/pixabay.list.item.dart';
import 'package:flutterscaffold/services/loading.dart';
import 'package:flutterscaffold/services/translation.dart';
import 'package:flutterscaffold/tools/text.dart';
import 'package:mvcprovider/mvcprovider.dart';

import 'pixabay.list.ctrl.dart';
import 'pixabay.list.model.dart';

class PixabayListView extends StatelessWidget with MVC_View<PixabayListModel, PixabayListCtrl> {

  @override
  Widget build(BuildContext context) {
    listen(context);

    Widget mainContent = model.images != null && model.images.length > 0 ? SliverList(
      delegate: SliverChildBuilderDelegate(
              (context, index) => PixabayListItem(model.images[index]),
          childCount: model.images.length
      ),
    ) : SliverFillRemaining(
      child: !LoadingService(context).listen.loading(model.loadingId) ? Center(
        child: Padding(
          padding: ThemeSizes.padding[Sizes.L],
          child: SubtitleLabel(
            TranslationService(context).get["No image found with this tag"],
            maxLines: 3,
          ),
        ),
      ) : Container(),
    );

    return SliverView(
      refreshKey: ctrl.refreshKey,
      overlap: !model.showAppBar,
      key: PageStorageKey(model.query), // To remember scroll position
      slivers: <Widget>[
        if (model.showAppBar) CustomSliverBar(
          title: SubtitleLabel(TextTool.capitalize(model.query), heroTag: "TITLE-${model.query}"),
        ),
        mainContent,
      ],
      onRefresh: () => ctrl.requestImages(true),
    );
  }
}